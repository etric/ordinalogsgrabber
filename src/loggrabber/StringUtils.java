package loggrabber;


/* stolen from org.apache.commons.lang.StringUtils */
final class StringUtils {

    private StringUtils() {}

    static final String EMPTY = "";

    static boolean isBlank(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((Character.isWhitespace(str.charAt(i)) == false)) {
                return false;
            }
        }
        return true;
    }
}
