package loggrabber;

import com.jcraft.jsch.*;
import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


class GetLogTask extends Task<ByteBuffer> {

    private static final Logger logger = LoggerFactory.getLogger(GetLogTask.class);

    private static final int CONNECTION_TIMEOUT_MILLIS = 5000;
    private static final String LOCAL_HOST = "127.0.0.1";
    private static final String DEV_BASTION_HOST = "dev-bastion.rijksoverheid.nl";
    private static final String SYSLOG_HOST = "syslog";
    private static final String SYSLOG_USER = "rijksoverheid";
    private static final int DEFAULT_SSH_PORT = 22;
    private static final String PRIVATE_KEY_FILE_NAME = "id_dsa";
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    static {
        JSch.setLogger(new JSchLoggerAdapter(LoggerFactory.getLogger("JSchLogger")));
    }

    private final JSch jsch;
    private final String login;
    private final String password;
    private final TargetLogs.Entry targetLog;

    GetLogTask(String login, String password, TargetLogs.Entry targetLog) {
        jsch = new JSch();
        this.login = login;
        this.password = password;
        this.targetLog = targetLog;
    }

    @Override public ByteBuffer call() throws Exception {
        Session devBastionSession = null;
        Session syslogSession = null;
        ByteBuffer byteBuffer = null;

        try {
            updateMessage("Connecting to " + DEV_BASTION_HOST + ":" + DEFAULT_SSH_PORT + " as " + login);
            devBastionSession = connectToDevBastion(login, password);

            updateMessage("Retrieving private key for " + login);
            downloadPrivateKey(devBastionSession);

            updateMessage("Setting up port forwarding for " + SYSLOG_HOST + " server");
            int assignedLocalPort = devBastionSession.setPortForwardingL(0, SYSLOG_HOST, DEFAULT_SSH_PORT);

            updateMessage("Connecting to " + SYSLOG_HOST + ":" + DEFAULT_SSH_PORT +
                    " via " + LOCAL_HOST + ":" + assignedLocalPort);
            syslogSession = connectToSysLogViaLocalhost(assignedLocalPort);

            updateMessage("Getting log file");
            byteBuffer = downloadLogFile(syslogSession);

        } finally {
            updateMessage("Closing sessions");
            if (syslogSession != null) syslogSession.disconnect();
            if (devBastionSession != null) devBastionSession.disconnect();
        }

        updateMessage("Transferring log file");
        return byteBuffer;
    }

    private Session connectToDevBastion(String login, String password) throws JSchException {
        Session session = jsch.getSession(login, DEV_BASTION_HOST, DEFAULT_SSH_PORT);
        session.setPassword(password);
        session.setHostKeyAlias(DEV_BASTION_HOST);
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect(CONNECTION_TIMEOUT_MILLIS);
        return session;
    }

    private Session connectToSysLogViaLocalhost(int assignedLocalPort) throws JSchException {
        Session session = jsch.getSession(SYSLOG_USER, LOCAL_HOST, assignedLocalPort);
        session.setHostKeyAlias(SYSLOG_HOST);
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect(CONNECTION_TIMEOUT_MILLIS);
        return session;
    }

    private void downloadPrivateKey(Session session) throws JSchException, SftpException {
        ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
        sftpChannel.connect(CONNECTION_TIMEOUT_MILLIS);
        String remoteFilePath = "/home/ro-users/" + login + "/.ssh/" + PRIVATE_KEY_FILE_NAME;
        sftpChannel.get(remoteFilePath, PRIVATE_KEY_FILE_NAME);
        sftpChannel.disconnect();
        jsch.addIdentity(PRIVATE_KEY_FILE_NAME);
    }

    private ByteBuffer downloadLogFile(Session parentSession) throws JSchException, SftpException {
        ChannelSftp sftpChannel = (ChannelSftp) parentSession.openChannel("sftp");
        sftpChannel.connect(CONNECTION_TIMEOUT_MILLIS);
        String remoteFilePath = "/var/log/rijksoverheid/" + targetLog.getServer() + "/" +
                DATE_FORMAT.format(new Date()) + "/" + targetLog.getLogFile();
        ByteArrayOutputStream remoteFileByteArrayOutputStream = new ByteArrayOutputStream();
        sftpChannel.get(remoteFilePath, remoteFileByteArrayOutputStream);
        sftpChannel.disconnect();
        return ByteBuffer.wrap(remoteFileByteArrayOutputStream.toByteArray());
    }

    @Override protected void updateMessage(String s) {
        super.updateMessage(s);
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            logger.warn("", e);
        }
    }
}
