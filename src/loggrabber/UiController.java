package loggrabber;

import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.prefs.Preferences;


public class UiController implements Initializable {

    private static final Logger logger = LoggerFactory.getLogger(UiController.class);

    private static final String DEFAULT_LOGIN = "";
    private static final String DEFAULT_PASSWORD = "";

    private static final String PREFERENCES_LOGIN = "login";
    private static final String PREFERENCES_PASSWORD = "password";
    private static final String PREFERENCES_TARGET_LOG = "targetLog";
    private static final int STATUS_BAR_HEIGHT = 30;
    private static final String STYLE_CLASS_VALID = "valid";
    private static final String STYLE_CLASS_INVALID = "invalid";

    @FXML private TextField loginTextField;
    @FXML private TextField passwordTextField;
    @FXML private ComboBox<TargetLogs.Entry> siteComboBox;
    @FXML private Button getLogsButton;
    @FXML private Text statusText;

    private Preferences preferences;

    @Override public void initialize(URL url, ResourceBundle resourceBundle) {
        assert siteComboBox != null;
        assert loginTextField != null;
        assert passwordTextField != null;
        assert getLogsButton != null;
        assert statusText != null;

        preferences = Preferences.userRoot().node(getClass().getName());

        loadPreferences();
    }

    @FXML void getLogsButtonClick() {
        if (isInputFieldsValidationOk()) {
            final String login = loginTextField.getText();
            final String password = passwordTextField.getText();
            final TargetLogs.Entry targetLog = siteComboBox.getValue();

            Task task = new GetLogTask(login, password, targetLog);
            statusText.textProperty().bind(task.messageProperty());

            showStatusBar(true);
            disableControls(true);

            task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent workerStateEvent) {
                    ByteBuffer byteBuffer = (ByteBuffer) workerStateEvent.getSource().getValue();
                    String fileName = targetLog.getServer() + "-" + targetLog.getLogFile();
                    if (saveLogContentsToFile(byteBuffer, fileName)) {
                        Dialogs.showInformationDialog(null, "Success! " +
                                "Log file has been saved to root directory as " + fileName);
                    } else {
                        Dialogs.showErrorDialog(null, "Failed saving log to file");
                    }
                    savePreferences(login, password, targetLog);
                    disableControls(false);
                    showStatusBar(false);
                }
            });

            task.setOnFailed(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent workerStateEvent) {
                    Throwable ex = workerStateEvent.getSource().getException();
                    logger.error("Failed getting logs from remote server", ex);
                    Dialogs.showErrorDialog(null, ex.getMessage(), "See logs for details", "Error", ex);
                    disableControls(false);
                    showStatusBar(false);
                }
            });

            Thread taskThread = new Thread(task);
            taskThread.setDaemon(true);
            taskThread.start();
        }
    }

    private void savePreferences(String login, String password, TargetLogs.Entry targetLog) {
        preferences.put(PREFERENCES_LOGIN, login);
        preferences.put(PREFERENCES_PASSWORD, password);
        preferences.put(PREFERENCES_TARGET_LOG, targetLog.toString());
    }

    private void loadPreferences() {
        loginTextField.setText(preferences.get(PREFERENCES_LOGIN, DEFAULT_LOGIN));
        passwordTextField.setText(preferences.get(PREFERENCES_PASSWORD, DEFAULT_PASSWORD));
        siteComboBox.getItems().setAll(TargetLogs.values());
        String targetLogString = preferences.get(PREFERENCES_TARGET_LOG, StringUtils.EMPTY);
        TargetLogs.Entry targetLog = TargetLogs.get(targetLogString);
        if (targetLog != null) {
            siteComboBox.setValue(targetLog);
        } else {
            siteComboBox.setValue(TargetLogs.DEFAULT_ENTRY);
        }
    }

    private void disableControls(boolean flag) {
        loginTextField.setDisable(flag);
        passwordTextField.setDisable(flag);
        siteComboBox.setDisable(flag);
        getLogsButton.setDisable(flag);
    }

    private boolean isInputFieldsValidationOk() {
        boolean isValidationOk = true;
        if (StringUtils.isBlank(loginTextField.getText())) {
            highlightInvalidField(loginTextField, true);
            loginTextField.requestFocus();
            isValidationOk = false;
        } else {
            highlightInvalidField(loginTextField, false);
        }
        if (StringUtils.isBlank(passwordTextField.getText())) {
            highlightInvalidField(passwordTextField, true);
            if (isValidationOk) passwordTextField.requestFocus();
            isValidationOk = false;
        } else {
            highlightInvalidField(passwordTextField, false);
        }
        return isValidationOk;
    }

    private void highlightInvalidField(Control field, boolean isInvalid) {
        List<String> styleClasses = field.getStyleClass();
        String styleClass2remove = isInvalid ? STYLE_CLASS_VALID : STYLE_CLASS_INVALID;
        String styleClass2add = isInvalid ? STYLE_CLASS_INVALID : STYLE_CLASS_VALID;
        if (!styleClasses.contains(styleClass2add)) {
            styleClasses.add(styleClass2add);
        }
        if (styleClasses.contains(styleClass2remove)) {
            styleClasses.remove(styleClass2remove);
        }
    }

    private boolean saveLogContentsToFile(ByteBuffer byteBuffer, String fileName) {
        if (byteBuffer == null || byteBuffer.array() == null ||
                byteBuffer.array().length == 0) {
            logger.error("No log data has been received");
            return false;
        }
        try {
            FileUtils.writeByteArrayToFile(new File(fileName), byteBuffer.array());
        } catch (IOException ex) {
            logger.error("Failed saving log to file", ex);
            return false;
        }
        return true;
    }

    private void showStatusBar(boolean flag) {
        if (flag) {
            new ResizeWindowHeightAnimation(Duration.millis(300),
                    Main.stage, STATUS_BAR_HEIGHT).play();
        } else {
            new ResizeWindowHeightAnimation(Duration.millis(300),
                    Main.stage, -STATUS_BAR_HEIGHT).play();
            Main.stage.setHeight(Main.WINDOW_HEIGHT); // to ensure
        }
    }
}
