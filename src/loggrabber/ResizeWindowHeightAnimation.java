package loggrabber;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.stage.Stage;
import javafx.util.Duration;


class ResizeWindowHeightAnimation extends Transition {

    private final Stage stage;
    private final double initialHeight;
    private final int deltaHeight;

    ResizeWindowHeightAnimation(Duration duration, Stage stage, int deltaHeight) {
        this.stage = stage;
        this.initialHeight = stage.getHeight();
        this.deltaHeight = deltaHeight;

        setCycleDuration(duration);
        setInterpolator(Interpolator.LINEAR);
        setCycleCount(1);
    }

    protected void interpolate(double k) {
        int tmpDeltaHeight = (int) Math.floor(k * deltaHeight);
        stage.setHeight(initialHeight + tmpDeltaHeight);
    }
}