package loggrabber;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;


final class TargetLogs {

    static final TargetLogs.Entry DEFAULT_ENTRY = TargetLogs.valueOf("ps-t-site1(site.log)");

    private static final String TARGET_LOG_ENTRIES_FILE = "targetlogs";
    private static final Logger logger = LoggerFactory.getLogger(TargetLogs.class);
    private static Map<String, TargetLogs.Entry> values;

    static TargetLogs.Entry valueOf(String stringValue) {
        checkInit();
        if (StringUtils.isBlank(stringValue)) {
            throw new IllegalArgumentException("Input string is blank");
        }
        TargetLogs.Entry targetLogEntry = values.get(stringValue);
        if (targetLogEntry != null) {
            return targetLogEntry;
        }
        int idxOpenBrace = stringValue.indexOf("(");
        int idxCloseBrace = stringValue.indexOf(")");
        if (idxOpenBrace == -1 || idxCloseBrace == -1) {
            throw new IllegalArgumentException("Invalid string format. " +
                    "Must be: <servername>(<logfilename>)");
        }
        String server = stringValue.substring(0, stringValue.indexOf("("));
        String logFile = stringValue.substring(stringValue.indexOf("(") + 1,
                stringValue.indexOf(")"));
        if (StringUtils.isBlank(server) || StringUtils.isBlank(logFile)) {
            throw new IllegalArgumentException("Invalid string. " +
                    "Either server or logfile is not specified");
        }
        targetLogEntry = new TargetLogs.Entry();
        targetLogEntry.server = server;
        targetLogEntry.logFile = logFile;
        targetLogEntry.asString = stringValue;
        return targetLogEntry;
    }

    static void add(String stringValue) {
        checkInit();
        values.put(stringValue, valueOf(stringValue));
    }

    static TargetLogs.Entry get(String str) {
        checkInit();
        return values.get(str);
    }

    static Collection<TargetLogs.Entry> values() {
        checkInit();
        return Collections.unmodifiableCollection(values.values());
    }

    private static void checkInit() {
        if (values == null) {
            values = new LinkedHashMap<String, Entry>();
            try {
                List<String> targetLogs =
                        FileUtils.readLines(new File(TARGET_LOG_ENTRIES_FILE));
                for (String entry : targetLogs) {
                    TargetLogs.add(entry);
                }
            } catch (Exception e) {
                logger.error("Failed parsing file with target log entries", e);
                values.put(DEFAULT_ENTRY.asString, DEFAULT_ENTRY);
            }
        }
    }


    static final class Entry {

        private String asString;
        private String server;
        private String logFile;

        String getServer() {
            return server;
        }

        String getLogFile() {
            return logFile;
        }

        @Override public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Entry entry = (Entry) o;
            if (!logFile.equals(entry.logFile)) return false;
            if (!server.equals(entry.server)) return false;

            return true;
        }

        @Override public int hashCode() {
            int result = server.hashCode();
            result = 31 * result + logFile.hashCode();
            return result;
        }

        @Override public String toString() {
            return asString;
        }
    }
}
