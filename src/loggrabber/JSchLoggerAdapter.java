package loggrabber;

import com.jcraft.jsch.Logger;


class JSchLoggerAdapter implements Logger {

    private final org.slf4j.Logger logger;

    public JSchLoggerAdapter(org.slf4j.Logger logger) {
        this.logger = logger;
    }

    @Override public boolean isEnabled(int i) {
        switch (i) {
            case Logger.DEBUG: return logger.isDebugEnabled();
            case Logger.INFO: return logger.isInfoEnabled();
            case Logger.WARN: return logger.isWarnEnabled();
            case Logger.ERROR: return logger.isErrorEnabled();
            case Logger.FATAL: return logger.isErrorEnabled();
        }
        return true;
    }

    @Override public void log(int i, String s) {
        switch (i) {
            case Logger.DEBUG: logger.debug(s); return;
            case Logger.INFO: logger.info(s); return;
            case Logger.WARN: logger.warn(s); return;
            case Logger.ERROR: logger.error(s); return;
            case Logger.FATAL: logger.error(s); return;
        }
        logger.trace(s);
    }
}