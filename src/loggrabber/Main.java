package loggrabber;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.util.ResourceBundle;


public class Main extends Application {

    static final String UI_FXML_FILE_RESOURCE = "res/ui.fxml";
    static final String UI_CSS_FILE_PATH = "loggrabber/res/ui.css";
    static final String RESOURCE_BUNDLE_NAME = "props";
    static final String APPLICATION_TITLE = "Ordina Logs Grabber";
    static final String APPLICATION_ICON_FILE_PATH = "loggrabber/res/app_icon.png";
    static final int WINDOW_WIDTH = 275;
    static final int WINDOW_HEIGHT = 222;

    static Stage stage;

    @Override public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(UI_FXML_FILE_RESOURCE),
                ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME));
        root.getStylesheets().add(UI_CSS_FILE_PATH);

        primaryStage.getIcons().add(new Image(APPLICATION_ICON_FILE_PATH));
        primaryStage.setTitle(APPLICATION_TITLE);
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT));
        primaryStage.show();
        primaryStage.sizeToScene();
        primaryStage.centerOnScreen();

        stage = primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
